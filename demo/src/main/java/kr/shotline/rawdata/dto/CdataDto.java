package kr.shotline.rawdata.dto;

import java.util.Date;

public interface CdataDto {
	Long getId();
	
	String getCounterId();

	String getTerminalId();

	String getShotCount();

	String getShotLastAt();

	String getShotReceivedAt();

	String getCommType();

	String getBatteryState();

	String getCycleTime();

	String getCt0();

	String getCt1();

	String getCt2();

	String getCt3();

	String getCt4();

	String getCt5();

	String getCt6();

	String getCt7();

	String getCt8();

	String getCt9();

	String getShot0();

	String getShot1();

	String getShot2();

	String getShot3();

	String getShot4();

	String getShot5();

	String getShot6();

	String getShot7();

	String getShot8();

	String getShot9();

	String getTemp1();

	String getTemp2();

	String getTemp3();

	String getTemp4();

	String getTemp5();

	String getTempCurrent();

	String getTempFirstAt();

	Integer getRmiCount();

	Integer getRmiAccumTime();

	String getRmiFirstDetachedAt();

	String getRmiLastAttachedAt();

	String getRmiAttached();

	Integer getShockCount();

	String getExternalPower();

	Integer getRouterCount();

	String getTerminalVer();

	Integer getSn();

	Date getCreatedAt();
}
