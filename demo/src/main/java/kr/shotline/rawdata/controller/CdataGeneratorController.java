package kr.shotline.rawdata.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import kr.shotline.rawdata.dto.CdataDto;
import kr.shotline.rawdata.entity.RequestCdata;
import kr.shotline.rawdata.service.impl.RawDataServiceImpl;
@RestController
public class CdataGeneratorController {

	// baseCt 정수
	private static final int BASE_CT_NUMBER = 13;

	@Autowired
	RawDataServiceImpl rawDataServiceImpl;

	// 마지막 통신시간 Cdata
	@GetMapping("/lastcdata")
	ResponseEntity<?> lastCdata() {

		List<CdataDto> dbLastCdata = rawDataServiceImpl.findLastShotReceivedAt();

		return ResponseEntity.ok(dbLastCdata);
	}

	@GetMapping("/cdata/{date}/{dayrange}")
	List<RequestCdata> makeCdata(@PathVariable("date") String startdate, @PathVariable("dayrange") int dayrange,
			@RequestParam(value = "shotlist", required = false) ArrayList<String> lastShotList,
			@RequestParam(value = "ctlist", required = false) ArrayList<String> LastCtList) {

		int[] shotList = new int[20];
		// baseCt 소수
		int[] baseCtDecimalPoint = new int[20];

		if (lastShotList.size() > 0) {
			for (int n = 0; n < shotList.length; n++) {
				shotList[n] = Integer.parseInt(lastShotList.get(n));
			}
		}

		int dayRange = dayrange;

		String originTime = "20220317040000";
		String oneDayTime = "";
		if (startdate != null) {
			originTime = startdate + "090000";
		}
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");

		int shot;

		int serialNumber = 0;

		// 카운터 ID
		String[] counterId = { "CCM2222P01001", "CCM2222P01002", "CCM2222P01003", "CCM2222P01004", "CCM2222P01005",
				"CCM2222P01006", "CCM2222P01007", "CCM2222P01008", "CCM2222P01009", "CCM2222P01010", "CCM2222P01011",
				"CCM2222P01012", "CCM2222P01013", "CCM2222P01014", "CCM2222P01015", "CCM2222P01016", "CCM2222P01017",
				"CCM2222P01018", "CCM2222P01019", "CCM2222P01020" };

		List<String> timeList = new ArrayList<>();

		int ctMaxCounter;
		// C/T 범위
		int errorRange;

		// CT list
		List<String> ctDatas = new ArrayList<>();
		// ct 중복제거
		Set<String> ctDataSet = new TreeSet<String>();
		// final ct/shot row data
		Map<String, Integer> ctAndShotRawdatas = new HashMap<>();

		// tempRawdatas
		List<RequestCdata> sampleRawdatas = new ArrayList<>();

		// 1시간 차이
		double timeGap = 0;
		double dayTimeGap = 0;

		// 일
		for (int d = 0; d < dayRange; d++) {
			Date dayDate = null;
			try {
				dayDate = simpleDateFormat.parse(originTime);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			Calendar dayCal = Calendar.getInstance();
			dayCal.setTime(dayDate);

			if (d > 0) {
				dayTimeGap += 86400.000;
				dayCal.add(Calendar.SECOND, (int) dayTimeGap);
			}

			oneDayTime = simpleDateFormat.format(dayCal.getTime());

			//시간
			for (int t = 0; t < 8; t++) {
				Date date = null;
				try {
					date = simpleDateFormat.parse(oneDayTime);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				Calendar cal = Calendar.getInstance();
				cal.setTime(date);

				timeGap = 3600.000;
				cal.add(Calendar.SECOND, (int) timeGap);

				oneDayTime = simpleDateFormat.format(cal.getTime());
				for (int a = 0; a < 20; a++) {
					timeList.add(oneDayTime);
				}

				// 카운터ea
				for (int i = 0; i < 20; i++) {
					for (int n = 0; n < 20; n++) {
						// 0~4
						baseCtDecimalPoint[n] = (int) ((Math.random() * 5) + 0);
					}

					// 0~4
					ctMaxCounter = (int) ((Math.random() * 4));

					// baseCt
					int baseCt = Integer.valueOf(BASE_CT_NUMBER + "" + baseCtDecimalPoint[i]);

					// 찍을shot = 1시간/baseCt
					shot = (int) Math.ceil(36000 / baseCt);

					// shot수만큼 ct 생성
					for (int j = 0; j < shot; j++) {
						// 0 ~ ctMaxCounter
						errorRange = (int) ((Math.random() * ctMaxCounter));

						if ((int) (Math.random() * 10) % 2 == 0) {
							ctDatas.add(Integer.toString(baseCt + errorRange));
						} else {
							ctDatas.add(Integer.toString(baseCt - errorRange));
						}
					}

					// ct값 중복 제거
					for (int h = 0; h < ctDatas.size(); h++) {
						ctDataSet.add(ctDatas.get(h));
					}

					// (ct, shot) 생성
					int counter;
					for (String ctTemp : ctDataSet) {
						counter = 0;
						for (String ctData : ctDatas) {
							if (ctTemp.equals(ctData)) {
								counter++;
							}
						}
						ctAndShotRawdatas.put(ctTemp, counter);
					}

					int totalCountTime = 0;

					// 생성된 (ct,shot)기준 카운트 시간 계산
					for (String key : ctAndShotRawdatas.keySet()) {
						totalCountTime += Integer.valueOf(key) * ctAndShotRawdatas.get(key);
					}

					// 카운트시간 오차 수정
					int subtractCount = 0;
					if (totalCountTime > 36000) {
						do {
							// 계산된 실제 카운트 시간 - 기준 ct값 => 기준ct shot -1
							totalCountTime -= (int) (baseCt);
							ctAndShotRawdatas.put(String.valueOf((int) (baseCt)),
									ctAndShotRawdatas.get(String.valueOf((int) (baseCt))) - 1);
							// 수정된 시간 횟수
							subtractCount++;
						} while (totalCountTime > 36000);
						// shotcounte - 수정된 시간횟수
						shot -= subtractCount;
					}
					// shotcount
					shotList[i] += shot;

					// 대표ct
					Integer maxShotValue = Collections.max(ctAndShotRawdatas.values());
					String maxCtValue = "0";
					for (Map.Entry<String, Integer> m : ctAndShotRawdatas.entrySet()) {
						if (m.getValue() == maxShotValue) {
							maxCtValue = m.getKey();
						}
					}

					// lastshotat생성
					Date tmplastShotTime = null;
					Date tmpTempTime = null;
					try {
						tmplastShotTime = simpleDateFormat.parse(timeList.get(i));
						tmpTempTime = simpleDateFormat.parse(timeList.get(i));
					} catch (ParseException e) {
						e.printStackTrace();
					}
					cal.setTime(tmplastShotTime);
					cal.add(Calendar.SECOND, -3600);
					cal.add(Calendar.SECOND, (int) (totalCountTime * 0.1));
					String lastShotTime = simpleDateFormat.format(cal.getTime());

					cal.setTime(tmpTempTime);
					cal.add(Calendar.SECOND, ((int) ((Math.random() * 5) + 1)) * -1);
					String tempTime = simpleDateFormat.format(cal.getTime());

					// serialNumber
					serialNumber += 2;

					// 온도 data 생성
					String tempCurrent = "";
					String[] tempList = new String[5];
					for (int a = 0; a < 6; a++) {
						int baseTemp = 20;
						int tmepDecimalPoint = (int) (Math.random() * 10);

						if (tmepDecimalPoint % 2 == 0) {
							baseTemp += (int) ((Math.random() * 3) + 1);
						} else {
							baseTemp += (int) ((Math.random() * 3) + 1);
						}

						if (a == 5) {
							tempCurrent = baseTemp + "" + tmepDecimalPoint;
						} else {
							tempList[a] = baseTemp + "" + tmepDecimalPoint;
						}
					}

					// @formatter:off
					RequestCdata requestCdata = RequestCdata.builder()
							.terminalId("CTM2222KRC222")
							.counterId(counterId[i])
							.shotCount(String.valueOf(shotList[i]))
							.shotLastAt(lastShotTime)
							.shotReceivedAt(timeList.get(i))
							.commType(i % 6 == 0 ? "M" : "N")
							.cycleTime(maxCtValue)
							.batteryState("H")
							.ct0("*")
							.ct1("*")
							.ct2("*")
							.ct3("*")
							.ct4("*")
							.ct5("*")
							.ct6("*")
							.ct7("*")
							.ct8("*")
							.ct9("*")
							.shot0("*")
							.shot1("*")
							.shot2("*")
							.shot3("*")
							.shot4("*")
							.shot5("*")
							.shot6("*")
							.shot7("*")
							.shot8("*")
							.shot9("*")
							.rmiCount("0")
							.rmiAccumTime("0")
							.rmiFirstDetachedAt("*")
							.rmiLastAttachedAt("*")
							.rmiHasAttached("Y")
							.temporary1(tempList[0])
							.temporary2(tempList[1])
							.temporary3(tempList[2])
							.temporary4(tempList[3])
							.temporary5(tempList[4])
							.temporaryFirstAt(tempTime)
							.temporaryCurrent(tempCurrent)
							.eiShockCount("0")
							.eiHasExternalPower("N")
							.seqNo(String.valueOf(serialNumber))
							.build();
					
					// @formatter:on

					int num = 0;
					for (String key : ctAndShotRawdatas.keySet()) {
						if (key != null) {
							num++;
							if (num == 1) {
								requestCdata.setCt0(key);
								requestCdata.setShot0(String.valueOf(ctAndShotRawdatas.get(key)));
							} else if (num == 2) {
								requestCdata.setCt1(key);
								requestCdata.setShot1(String.valueOf(ctAndShotRawdatas.get(key)));
							} else if (num == 3) {
								requestCdata.setCt2(key);
								requestCdata.setShot2(String.valueOf(ctAndShotRawdatas.get(key)));
							} else if (num == 4) {
								requestCdata.setCt3(key);
								requestCdata.setShot3(String.valueOf(ctAndShotRawdatas.get(key)));
							} else if (num == 5) {
								requestCdata.setCt4(key);
								requestCdata.setShot4(String.valueOf(ctAndShotRawdatas.get(key)));
							} else if (num == 6) {
								requestCdata.setCt5(key);
								requestCdata.setShot5(String.valueOf(ctAndShotRawdatas.get(key)));
							} else if (num == 7) {
								requestCdata.setCt6(key);
								requestCdata.setShot6(String.valueOf(ctAndShotRawdatas.get(key)));
							} else if (num == 8) {
								requestCdata.setCt7(key);
								requestCdata.setShot7(String.valueOf(ctAndShotRawdatas.get(key)));
							} else if (num == 9) {
								requestCdata.setCt8(key);
								requestCdata.setShot8(String.valueOf(ctAndShotRawdatas.get(key)));
							} else if (num == 10) {
								requestCdata.setCt9(key);
								requestCdata.setShot9(String.valueOf(ctAndShotRawdatas.get(key)));
							}
						}
					}

					sampleRawdatas.add(requestCdata);

					cal.clear();
					ctAndShotRawdatas.clear();
					ctDatas.clear();
					ctDataSet.clear();
				}
				timeList.clear();
			}
		}

		return sampleRawdatas;
	}

	@PostMapping("/rawdatas")
	public ResponseEntity<?> sendRawDatas(@RequestBody List<RequestCdata> cdataList) {
		List<RequestCdata> tmpdata = rawDataServiceImpl.saveRawDatas(cdataList);
		return ResponseEntity.ok(tmpdata);
	}

	@GetMapping("/terminal/{terminalId}/rawdata")
	public ResponseEntity<?> getRawDatas(@PathVariable("terminalId") String terminalId,
			@RequestParam(value = "start", required = false) String start,
			@RequestParam(value = "end", required = false) String end) {
		List<CdataDto> rawCdatas = rawDataServiceImpl.rawDataBuTerminlId(terminalId, start, end);
		return ResponseEntity.ok(rawCdatas);
	}

	@DeleteMapping("/rawdatas/{start}/{end}")
	public ResponseEntity<?> deleteRawDate(@PathVariable("start") String start, @PathVariable("end") String end) {
		return ResponseEntity.ok(rawDataServiceImpl.deleteRawData(start, end));
	}

}
