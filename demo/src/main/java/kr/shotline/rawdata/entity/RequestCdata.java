package kr.shotline.rawdata.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Builder
@Data
@Table(name = "tb_request_cdata")
@NoArgsConstructor
@AllArgsConstructor
public class RequestCdata {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "terminal_id")
	private String terminalId;
	
	@Column(name = "counter_id")
	private String counterId;

	@Column(name = "shot_count")
	private String shotCount;

	@Column(name = "shot_last_at")
	private String shotLastAt;

	@Column(name = "shot_received_at")
	private String shotReceivedAt;

	@Column(name = "ci_comm_type")
	private String commType;

	@Column(name = "ci_cycle_time")
	private String cycleTime;

	@Column(name = "ci_battery_state")
	private String batteryState;

	//ctt
	@Column(name = "ctt_ct_0")
	private String ct0;
	@Column(name = "ctt_shot_0")
	private String shot0;
	
	@Column(name = "ctt_ct_1")
	private String ct1;
	@Column(name = "ctt_shot_1")
	private String shot1;
	
	@Column(name = "ctt_ct_2")
	private String ct2;
	@Column(name = "ctt_shot_2")
	private String shot2;
	
	@Column(name = "ctt_ct_3")
	private String ct3;
	@Column(name = "ctt_shot_3")
	private String shot3;
	
	@Column(name = "ctt_ct_4")
	private String ct4;
	@Column(name = "ctt_shot_4")
	private String shot4;
	
	@Column(name = "ctt_ct_5")
	private String ct5;
	@Column(name = "ctt_shot_5")
	private String shot5;
	
	@Column(name = "ctt_ct_6")
	private String ct6;
	@Column(name = "ctt_shot_6")
	private String shot6;
	
	@Column(name = "ctt_ct_7")
	private String ct7;
	@Column(name = "ctt_shot_7")
	private String shot7;
	
	@Column(name = "ctt_ct_8")
	private String ct8;
	@Column(name = "ctt_shot_8")
	private String shot8;
	
	@Column(name = "ctt_ct_9")
	private String ct9;
	@Column(name = "ctt_shot_9")
	private String shot9;
	
	@Column(name = "rmi_count")
	private String rmiCount;
	
	@Column(name = "rmi_accum_time")
	private String rmiAccumTime;
	
	@Column(name = "rmi_first_detached_at")
	private String rmiFirstDetachedAt;
	
	@Column(name = "rmi_last_attached_at")
	private String rmiLastAttachedAt;
	
	@Column(name = "rmi_attached")
	private String rmiHasAttached;
	
	@Column(name = "temp_1")
	private String temporary1;
	
	@Column(name = "temp_2")
	private String temporary2;
	
	@Column(name = "temp_3")
	private String temporary3;
	
	@Column(name = "temp_4")
	private String temporary4;
	
	@Column(name = "temp_5")
	private String temporary5;
	
	@Column(name = "temp_first_at")
	private String temporaryFirstAt;
	
	@Column(name = "temp_current")
	private String temporaryCurrent;
	
	@Column(name = "ei_shock_count")
	private String eiShockCount;
	
	@Column(name = "ei_external_power")
	private String eiHasExternalPower;

	@Column(name = "sn")
	private String seqNo;
	
}
