package kr.shotline.rawdata.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import kr.shotline.rawdata.dto.CdataDto;
import kr.shotline.rawdata.entity.RequestCdata;

@Repository
public interface RequestCdataRepository extends JpaRepository<RequestCdata, Long> {

	// @formatter:off

		public static final String RETREIVE_CDATA_BY_DATE = ""
				+ "SELECT rc.id AS id, "
				+ "       rc.terminal_id AS terminalId, "
//				+ "       rc.terminal_ver AS terminalVer, "
//				+ "       rc.router_count AS routerCount, "
				+ "       rc.counter_id AS counterId, "
				+ "       rc.shot_count AS shotCount, "
				+ "       STR_TO_DATE(rc.shot_last_at,'%Y%m%d%H%i%s') AS shotLastAt, "
				+ "       STR_TO_DATE(rc.shot_received_at,'%Y%m%d%H%i%s') AS shotReceivedAt, "
				+ "       rc.ci_comm_type AS commType, "
				+ "       rc.ci_battery_state AS batteryState, "
				+ "       CAST(rc.ci_cycle_time as UNSIGNED) * 0.1 AS cycleTime, "
				+ "       (CASE WHEN ctt_ct_0     = '*' THEN '*' ELSE CAST(ctt_ct_0     AS UNSIGNED) * 0.1 END) AS ct0, "
				+ "       (CASE WHEN ctt_ct_1     = '*' THEN '*' ELSE CAST(ctt_ct_1     AS UNSIGNED) * 0.1 END) AS ct1, "
				+ "       (CASE WHEN ctt_ct_2     = '*' THEN '*' ELSE CAST(ctt_ct_2     AS UNSIGNED) * 0.1 END) AS ct2, "
				+ "       (CASE WHEN ctt_ct_3     = '*' THEN '*' ELSE CAST(ctt_ct_3     AS UNSIGNED) * 0.1 END) AS ct3, "
				+ "       (CASE WHEN ctt_ct_4     = '*' THEN '*' ELSE CAST(ctt_ct_4     AS UNSIGNED) * 0.1 END) AS ct4, "
				+ "       (CASE WHEN ctt_ct_5     = '*' THEN '*' ELSE CAST(ctt_ct_5     AS UNSIGNED) * 0.1 END) AS ct5, "
				+ "       (CASE WHEN ctt_ct_6     = '*' THEN '*' ELSE CAST(ctt_ct_6     AS UNSIGNED) * 0.1 END) AS ct6, "
				+ "       (CASE WHEN ctt_ct_7     = '*' THEN '*' ELSE CAST(ctt_ct_7     AS UNSIGNED) * 0.1 END) AS ct7, "
				+ "       (CASE WHEN ctt_ct_8     = '*' THEN '*' ELSE CAST(ctt_ct_8     AS UNSIGNED) * 0.1 END) AS ct8, "
				+ "       (CASE WHEN ctt_ct_9     = '*' THEN '*' ELSE CAST(ctt_ct_9     AS UNSIGNED) * 0.1 END) AS ct9, "
				+ "       (CASE WHEN ctt_shot_0   = '*' THEN '*' ELSE CAST(ctt_shot_0   AS UNSIGNED)       END) AS shot0, "
				+ "       (CASE WHEN ctt_shot_1   = '*' THEN '*' ELSE CAST(ctt_shot_1   AS UNSIGNED)       END) AS shot1, "
				+ "       (CASE WHEN ctt_shot_2   = '*' THEN '*' ELSE CAST(ctt_shot_2   AS UNSIGNED)       END) AS shot2, "
				+ "       (CASE WHEN ctt_shot_3   = '*' THEN '*' ELSE CAST(ctt_shot_3   AS UNSIGNED)       END) AS shot3, "
				+ "       (CASE WHEN ctt_shot_4   = '*' THEN '*' ELSE CAST(ctt_shot_4   AS UNSIGNED)       END) AS shot4, "
				+ "       (CASE WHEN ctt_shot_5   = '*' THEN '*' ELSE CAST(ctt_shot_5   AS UNSIGNED)       END) AS shot5, "
				+ "       (CASE WHEN ctt_shot_6   = '*' THEN '*' ELSE CAST(ctt_shot_6   AS UNSIGNED)       END) AS shot6, "
				+ "       (CASE WHEN ctt_shot_7   = '*' THEN '*' ELSE CAST(ctt_shot_7   AS UNSIGNED)       END) AS shot7, "
				+ "       (CASE WHEN ctt_shot_8   = '*' THEN '*' ELSE CAST(ctt_shot_8   AS UNSIGNED)       END) AS shot8, "
				+ "       (CASE WHEN ctt_shot_9   = '*' THEN '*' ELSE CAST(ctt_shot_9   AS UNSIGNED)       END) AS shot9, "
				+ "       (CASE WHEN temp_1       = '*' THEN '*' ELSE CAST(temp_1       AS UNSIGNED) * 0.1 END) AS temp1, "
				+ "       (CASE WHEN temp_2       = '*' THEN '*' ELSE CAST(temp_2       AS UNSIGNED) * 0.1 END) AS temp2, "
				+ "       (CASE WHEN temp_3       = '*' THEN '*' ELSE CAST(temp_3       AS UNSIGNED) * 0.1 END) AS temp3, "
				+ "       (CASE WHEN temp_4       = '*' THEN '*' ELSE CAST(temp_4       AS UNSIGNED) * 0.1 END) AS temp4, "
				+ "       (CASE WHEN temp_5       = '*' THEN '*' ELSE CAST(temp_5       AS UNSIGNED) * 0.1 END) AS temp5, "
				+ "       (CASE WHEN temp_current = '*' THEN '*' ELSE CAST(temp_current AS UNSIGNED) * 0.1 END) AS tempCurrent, "
				+ "       (CASE WHEN temp_first_at IS NULL OR temp_first_at = '*' THEN '*' ELSE STR_TO_DATE(temp_first_at,'%Y%m%d%H%i%s') END) AS tempFirstAt, "
				+ "       IFNULL(rmi_count, '*') AS rmiCount, IFNULL(rmi_accum_time, '*') AS rmiAccumTime, "
				+ "       (CASE WHEN rmi_first_detached_at IS NULL OR rmi_first_detached_at = '*' THEN '*' ELSE STR_TO_DATE(rmi_first_detached_at,'%Y%m%d%H%i%s') END) AS rmiFirstDetachedAt, "
				+ "       (CASE WHEN rmi_last_attached_at  IS NULL OR rmi_last_attached_at  = '*' THEN '*' ELSE STR_TO_DATE(rmi_last_attached_at, '%Y%m%d%H%i%s') END) AS rmiLastAttachedAt, "
				+ "       rmi_attached AS rmiAttached, ei_shock_count AS shockCount, ei_external_power AS externalPower, sn, rc.created_at AS createdAt "
				+ "  FROM tb_request_cdata rc "
				;
		
		public static final String RETREIVE_LAST_SHOT_RECEIVED_AT_BY_COUNTER = ""
				+ "SELECT "
				+ "counter_id AS counterId, "
				+ "shot_count AS shotCount, "
				+ "shot_received_at AS shotReceivedAt, "
				+ "ci_cycle_time AS cycleTime "
				+ "FROM(SELECT * FROM tb_request_cdata WHERE(counter_id, shot_received_at) IN (SELECT counter_id, max(shot_received_at) as date_time FROM tb_request_cdata GROUP BY counter_id)ORDER BY shot_received_at DESC) t GROUP BY t.counter_id";

		//터미널 아이디 검색
		public static final String RETREIVE_CDATA_BY_TERMINAL_AND_CREATED_AT = ""
				+ RETREIVE_CDATA_BY_DATE
				+ "  WHERE shot_received_at >= :start "
				+ "    AND shot_received_at  <= :end "
				+ "    AND terminal_id = :terminalId "
				+ "  ORDER BY created_at DESC, sn DESC "
				;

		// @formatter:on

	@Query(value = RETREIVE_CDATA_BY_TERMINAL_AND_CREATED_AT, nativeQuery = true)
	List<CdataDto> findCdataByTerminalIdAndBetweenDate(@Param("terminalId") String terminalId,
			@Param("start") String start, @Param("end") String end);

	@Query(value = RETREIVE_LAST_SHOT_RECEIVED_AT_BY_COUNTER, nativeQuery = true)
	List<CdataDto> findLastShotReceivedAt();

	@Modifying
	@Query("delete from RequestCdata c where c.terminalId = ?1 and c.shotLastAt between ?2 and ?3")
	int deleteByTerminalIdAndShotLastAtBetween(String terminalId, String start, String end);

}
