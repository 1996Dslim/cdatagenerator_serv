package kr.shotline.rawdata.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.shotline.rawdata.dto.CdataDto;
import kr.shotline.rawdata.entity.RequestCdata;
import kr.shotline.rawdata.repository.RequestCdataRepository;
import kr.shotline.rawdata.service.RawDataService;

@Service
public class RawDataServiceImpl implements RawDataService {

	@Autowired
	RequestCdataRepository requestCdataRepository;
	
	@Transactional
	@Override
	//	@Async
	public List<RequestCdata> saveRawDatas(List<RequestCdata> rawDatas) {
		List<RequestCdata> tmpdata = requestCdataRepository.saveAllAndFlush(rawDatas);
		
		return tmpdata;
	}

	@Override
	public List<CdataDto> rawDataBuTerminlId(String terminalId, String start, String end) {
		return requestCdataRepository.findCdataByTerminalIdAndBetweenDate(terminalId, start, end);
	}
	
	@Override
	public List<CdataDto> findLastShotReceivedAt() {
		return requestCdataRepository.findLastShotReceivedAt();
	} 

	@Transactional
	@Override
	public boolean deleteRawData(String start, String end) {
		System.out.println(start + " : " + end);
		int cnt = requestCdataRepository.deleteByTerminalIdAndShotLastAtBetween("CTM2222KRC222", start, end);
		return cnt > 0;
	}

}
