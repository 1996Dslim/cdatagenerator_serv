package kr.shotline.rawdata.service;

import java.util.List;

import kr.shotline.rawdata.dto.CdataDto;
import kr.shotline.rawdata.entity.RequestCdata;

public interface RawDataService {
	List<RequestCdata> saveRawDatas(List<RequestCdata> rawDatas);
	List<CdataDto> rawDataBuTerminlId(String terminalId, String start, String end);
	List<CdataDto> findLastShotReceivedAt();
	boolean deleteRawData(String start, String end);
}
