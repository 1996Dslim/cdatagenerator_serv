package kr.shotline.test.rawdata;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import kr.shotline.rawdata.entity.RequestCdata;
import kr.shotline.rawdata.repository.RequestCdataRepository;

@SpringBootTest
public class RawDataInsertTest {
	
	@Autowired
	RequestCdataRepository requestCdataRepository; 
	
	@Test
	void rowDataSave() {
		
		RequestCdata requestCdata = RequestCdata.builder()
				.terminalId("LTM2020KRL001")
				.counterId("LCM2020M10001")
				.shotCount("486")
				.shotLastAt("20210315045957")
				.shotReceivedAt("20210315050000")
				.commType("N")
				.cycleTime("74")
				.batteryState("H")
				.rmiCount("0")
				.rmiAccumTime("0")
				.rmiFirstDetachedAt("19700101015959")
				.rmiLastAttachedAt("19700101015959")
				.rmiHasAttached("Y")
				.temporary1("219")
				.temporary2("232")
				.temporary3("222")
				.temporary4("222")
				.temporary5("235")
				.temporaryFirstAt("20210315045959")
				.temporaryCurrent("235")
				.eiShockCount("0")
				.eiHasExternalPower("N")
				.seqNo("2")
				.build();
		
		System.out.println("requestCdata" + requestCdata);
		
		requestCdataRepository.save(requestCdata);
	}
}
